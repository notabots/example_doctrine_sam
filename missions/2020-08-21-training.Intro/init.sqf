a = loadBtSet "o:\vbs_control_example_doctrine_sam\vbs_control_example_doctrine_sam.btset";

_soldiers = [
	ft1_1,
	ft1_2,
	ft1_3,
	ft1_4
];

// assign behavior to all soldiers
{
	_x setBT ["vbs_control_example_doctrine_sam", "genericRoot"];
} foreach _soldiers;

// assing behavior to the group
(group ft1_1) setBT ["vbs_control_example_doctrine_sam", "genericRoot"];

fn_orderMove = {
	_receiver = _this select 0;
	_position = _this select 1;
	
	_receiver receiveMessage [
		"NewOrder",
		[
			"name", "move", // mandatory
			
			// order specific values
			"position", _position
		]
	];
};

fn_orderFollow = {
	_receiver = _this select 0; // first argument of the function
	_entityToFollow = _this select 1; 
	
	_receiver receiveMessage [
		"NewOrder",
		[
			"name", "follow", // mandatory
			
			// order specific values
			"entityToFollow", _entityToFollow
		]
	];
};














