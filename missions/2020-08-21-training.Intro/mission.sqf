activateAddons [ 
  "vbs2_people_nl_army_soldier",
  "vbs2_plugins_training"
];

_missionVersion = 10;
setMissionVersion 10;
if !(isNil '_map') then
{
	call fn_vbs_editor_initGlobal;
	initAmbientLife;
};

_func_COC_Create_Unit = fn_vbs_editor_unit_create;
_func_COC_Update_Unit = fn_vbs_editor_unit_update;
_func_COC_Delete_Unit = fn_vbs_editor_unit_delete;
_func_COC_Import_Unit = fn_vbs_editor_unit_import;
_func_COC_UpdatePlayability_Unit = fn_vbs_editor_unit_updatePlayability;
_func_COC_Create_Group = fn_vbs_editor_group_create;
_func_COC_Update_Group = fn_vbs_editor_group_update;
_func_COC_Delete_Group = fn_vbs_editor_group_delete;
_func_COC_Delete_Group_Only = fn_vbs_editor_group_deleteOnlyGroup;
_func_COC_Attach_Group = fn_vbs_editor_group_attach;
_func_COC_Group_OnCatChanged = fn_vbs_editor_group_onCatChanged;
_func_COC_Group_OnTypeChanged = fn_vbs_editor_group_onTypeChanged;
_func_COC_Group_OnNewCatChanged = fn_vbs_editor_group_onNewCatChanged;
_func_COC_Group_OnNewTypeChanged = fn_vbs_editor_group_onNewTypeChanged;
_func_COC_Group_OnCreateInit = fn_vbs_editor_group_createOnInit;
_func_COC_Group_Selected = fn_vbs_editor_group_groupSelected;
_func_COC_SubTeam_Join = fn_vbs_editor_subteam_join;
_func_COC_Waypoint_Assign = fn_vbs_editor_waypoint_assign;
_func_COC_Waypoint_Update = fn_vbs_editor_waypoint_update;
_func_COC_Waypoint_Draw = fn_vbs_editor_waypoint_draw;
_func_COC_Waypoint_Delete = fn_vbs_editor_waypoint_delete;
_func_COC_Waypoint_Move = fn_vbs_editor_waypoint_move;
_func_COC_Waypoint_Load_Branched = fn_vbs_editor_waypoint_loadBranched;
_func_COC_Waypoint_Find_Config = fn_vbs_editor_waypoint_findConfigEntry;
_func_COC_Marker_Create = fn_vbs_editor_marker_create;
_func_COC_Marker_Update = fn_vbs_editor_marker_update;
_func_COC_Marker_SetDrawIcons = fn_vbs_editor_marker_setDrawIcons;
_func_COC_Marker_DlgChanged = fn_vbs_editor_marker_dlgChanged;
_func_COC_Marker_Tactical_Create = fn_vbs_editor_marker_tactical_create;
_func_COC_Marker_Tactical_Update = fn_vbs_editor_marker_tactical_update;
_func_COC_Marker_Tactical_SetDrawIcons = fn_vbs_editor_marker_tactical_setDrawIcons;
_getCrew = fn_vbs_editor_vehicle_getCrew;
_func_COC_Vehicle_Create = fn_vbs_editor_vehicle_create;
_func_COC_Vehicle_Update = fn_vbs_editor_vehicle_update;
_func_COC_Vehicle_Occupy = fn_vbs_editor_vehicle_occupy;
_func_COC_Vehicle_Delete = fn_vbs_editor_vehicle_delete;
_func_COC_Vehicle_UnJoin = fn_vbs_editor_vehicle_unJoinGroup;
_func_COC_Vehicle_GetInEH = fn_vbs_editor_vehicle_getInEH;
_func_COC_Vehicle_GetOutEH = fn_vbs_editor_vehicle_getOutEH;
_func_COC_Vehicle_OnTypeChanged = fn_vbs_editor_vehicle_onTypeChanged;
_func_COC_Vehicle_UpdatePlayability = fn_vbs_editor_vehicle_updatePlayability;
_func_COC_Import_Vehicle = fn_vbs_editor_vehicle_import;
_func_COC_Vehicle_Set_Arcs = fn_vbs_editor_vehicle_setArcs;
_func_COC_Trigger_SetDisplayName = fn_vbs_editor_trigger_setDisplayName;
_func_COC_Trigger_Create = fn_vbs_editor_trigger_create;
_func_COC_IED_Create = fn_vbs_editor_IED_create;
_func_COC_Set_Display_Names = fn_vbs_editor_setDisplayNames;
_func_COC_Set_Color = fn_vbs_editor_setColor;
_func_COC_PlaceObjOnObj = fn_vbs_editor_placeObjOnObj;
_func_COC_Draw_Distance = fn_vbs_editor_distance_draw;
_func_COC_LookAt_Create = fn_vbs_editor_lookAt_create;
editor_pausedQueues = getArray (configFile >> 'CfgEditorSettings' >> 'FunctionQueues' >> 'pausedQueuesOME');
{missionNamespace setVariable [_x, []]} forEach editor_pausedQueues;
editor_updateObjectTreeQueue = [];
editor_updateObjectTreeFull = true;
private '_allWaypoints';

_group_0 = ["_group_0","1-1-A-1",[2506.49196, 2599.64976, 13.86000],"WEST","","","",0,[],"",false,false,"m0_1",""] call fn_vbs_editor_group_create;

private ["_strCommander", "_strDriver", "_strGunner", "_strCargo"];
_strCommander = ""; _strDriver = ""; _strGunner = ""; _strCargo = "";
_azimuth = 0;
if (false) then
{
	_azimuth = 0;
};
_unit_1 = (
[
	"_unit_1", true, "VBS2_NL_Army_Leader_DW_C7", [2506.49196, 2599.64976, 13.86000], [], 0, "CAN_COLLIDE", _azimuth, "ft1_1", 1,
	1, -1, "UNKNOWN", "", "P2C", 1, _strCommander, _strDriver, _strGunner, _strCargo, "ActorAi", false, "_group_0", "west", "", [], "", "YELLOW", "SAFE", "Auto", 1,
	0.77778, 0.2, 0.51778, 0.2, [], "", [], 0.75, 1.82, 0, false, "", 1, 0, '', 1,
 5167
] + [_group_0]) call fn_vbs_editor_unit_create;

private ["_strCommander", "_strDriver", "_strGunner", "_strCargo"];
_strCommander = ""; _strDriver = ""; _strGunner = ""; _strCargo = "";
_azimuth = 0;
if (false) then
{
	_azimuth = 0;
};
_unit_2 = (
[
	"_unit_2", true, "VBS2_NL_Army_ATSoldier_DW_C7_PZF3", [2504.63119, 2595.90330, 13.86000], [], 0, "CAN_COLLIDE", _azimuth, "ft1_4", 1,
	1, -1, "UNKNOWN", "", "P2C", 1, _strCommander, _strDriver, _strGunner, _strCargo, "ActorAi", false, "_group_0", "west", "", [], "", "YELLOW", "SAFE", "Auto", 1,
	0.77778, 0.2, 0.51778, 0.2, [], "", [], 0.75, 1.82, 0, false, "", 1, 0, '_unit_1', 1,
 5172
] + [_group_0]) call fn_vbs_editor_unit_create;

private ["_strCommander", "_strDriver", "_strGunner", "_strCargo"];
_strCommander = ""; _strDriver = ""; _strGunner = ""; _strCargo = "";
_azimuth = 0;
if (false) then
{
	_azimuth = 0;
};
_unit_3 = (
[
	"_unit_3", true, "VBS2_NL_Army_MGunner_DW_MAG", [2506.91103, 2594.63036, 13.86000], [], 0, "CAN_COLLIDE", _azimuth, "ft1_3", 1,
	1, -1, "UNKNOWN", "", "P2C", 1, _strCommander, _strDriver, _strGunner, _strCargo, "ActorAi", false, "_group_0", "west", "", [], "", "YELLOW", "SAFE", "Auto", 1,
	0.77778, 0.2, 0.51778, 0.2, [], "", [], 0.75, 1.82, 0, false, "", 1, 0, '_unit_1', 1,
 5176
] + [_group_0]) call fn_vbs_editor_unit_create;

private ["_strCommander", "_strDriver", "_strGunner", "_strCargo"];
_strCommander = ""; _strDriver = ""; _strGunner = ""; _strCargo = "";
_azimuth = 0;
if (false) then
{
	_azimuth = 0;
};
_unit_4 = (
[
	"_unit_4", true, "VBS2_NL_Army_Grenadier_DW_C7UGL", [2509.59318, 2594.89549, 13.86000], [], 0, "CAN_COLLIDE", _azimuth, "ft1_2", 1,
	1, -1, "UNKNOWN", "", "P2C", 1, _strCommander, _strDriver, _strGunner, _strCargo, "ActorAi", false, "_group_0", "west", "", [], "", "YELLOW", "SAFE", "Auto", 1,
	0.77778, 0.2, 0.51778, 0.2, [], "", [], 0.75, 1.82, 0, false, "", 1, 0, '_unit_1', 1,
 5180
] + [_group_0]) call fn_vbs_editor_unit_create;

private ["_strCommander", "_strDriver", "_strGunner", "_strCargo"];
_strCommander = ""; _strDriver = ""; _strGunner = ""; _strCargo = "";
_azimuth = 0;
if (false) then
{
	_azimuth = 0;
};
_unit_0 = (
[
	"_unit_0", true, "VBS2_NL_Army_Leader_DW_C7", [2515.30171, 2590.40148, 13.86000], [], 0, "CAN_COLLIDE", _azimuth, "", 1,
	1, -1, "UNKNOWN", "", "P2C", 1, _strCommander, _strDriver, _strGunner, _strCargo, "Player", true, "", "WEST", "", [], "", "YELLOW", "SAFE", "Auto", 1,
	0.77778, 0.2, 0.51778, 0.2, [], "", [], 0.75, 1.82, 0, false, "", 1, 0, '', 1,
 5184
] + []) call fn_vbs_editor_unit_create;

_object_0 = objNull;
if (true) then
{
	_azimuth = 0;
	if (false) then
	{
		_azimuth = 0;
	};
  _object_0 = ["_object_0", false, "vbs2_visual_arrow_blue", [2512.19872, 2611.86931, 13.86000], [], 0, "CAN_COLLIDE", _azimuth, "arrowOne", 1, 1, 1, "UNKNOWN", "DEFAULT", "", 0, "", "", "NO CHANGE", "UNCHANGED", true, 1, "on", "off", [], [], [], [],[0,1,0], [0,0,1], ['container',"", 'offset',[0,0,0], 'rotated',"", 'stacked',"", 'hide',false, 'align_x',"", 'align_y',"", 'search_editor_args',""], ['V_ID','','V_TYPE', "", 'V_ACTION', "", 'V_POS','[0,0,0]', 'POS_ASL2', '[0,0,0]', 'V_VDIR','[0,0,0]', 'V_VUP','[0,0,1]', 'CLIPLAND', false,'V_SCALE','[1,1,1]'],"",""] call fn_vbs_editor_vehicle_create;
};

_trigger_0 = ["_trigger_0", [2528.21740, 2600.10312, 13.86000], [3, 3, 0, true], ["ALPHA", "PRESENT", true], [0, 0, 0, false], "NONE", "LeaderMove", "LeaderMove", ["this", "[ft1_1, (getPosASL2 arrowOne)] call fn_orderMove;", ""],objNull,["true","NoChange","NoChange","NoChange","NoChange","NoChange","None","plain","binocular","$NONE$", addQuotes ''],"", [false, '', true]] call '\vbs2\editor\Data\Scripts\trigger\createTrigger.sqf';

_trigger_2 = ["_trigger_2", [2534.88001, 2600.09254, 13.86000], [3, 3, 0, true], ["BRAVO", "PRESENT", true], [0, 0, 0, false], "NONE", "LeaderFollowMe", "LeaderFollowMe", ["this", "[ft1_1, player] call fn_orderFollow;", ""],objNull,["true","NoChange","NoChange","NoChange","NoChange","NoChange","None","plain","binocular","$NONE$", addQuotes ''],"", [false, '', true]] call '\vbs2\editor\Data\Scripts\trigger\createTrigger.sqf';

_trigger_5 = ["_trigger_5", [2541.54674, 2599.92782, 13.86000], [3, 3, 0, true], ["CHARLIE", "PRESENT", true], [0, 0, 0, false], "NONE", "TeamMove", "TeamMove", ["this", "[group ft1_1, (getPosASL2 arrowOne)] call fn_orderMove;", ""],objNull,["true","NoChange","NoChange","NoChange","NoChange","NoChange","None","plain","binocular","$NONE$", addQuotes ''],"", [false, '', true]] call '\vbs2\editor\Data\Scripts\trigger\createTrigger.sqf';

call fn_vbs_editor_group_finalizeGroups;
call fn_vbs_editor_waypoint_waypointsPrepare;
call fn_vbs_editor_waypoint_waypointsPrepareSynch;
{_x call fn_vbs_editor_functionQueueRun} forEach editor_pausedQueues;
editor_pausedQueues = nil;
editor_updateObjectTreeFull = nil;
editor_updateObjectTreeQueue = nil;
if (isNil '_map') then {processInitCommands};
